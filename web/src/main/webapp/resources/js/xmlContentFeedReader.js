
    $.ajax({
        url: "http://localhost:8080/jboss-news-reader-ear-web/rest/news/getAnnouncement/",
        dataType: "xml",
        success: function(data) {
            var $i = 0;
            $(data).find("atom\\:entry, entry").each(function() {
                $i++;
                var item = $(this);
                var date = $(item).find('atom\\:published, published').text();
                var pubDate = $(item).find('atom\\:published, published').text();
                var title = $(item).find('atom\\:title, title').eq(1).text();
                var description = $(item).find('atom\\:content, content').html();
                var html = generateHtml(title,date,pubDate,description, 1);
              $("#announcement-feeds").append(html);
            });
            $("#result").hide();
        }
    }); 
   

$(function(){
    $("p.modalTrigger").click(function() {
//        console.log(this.parent);
        $(this).parent().find("#myModal").modal('toggle');
    }); 
});
    $.ajax({
        url: "http://localhost:8080/jboss-news-reader-ear-web/rest/news/exportContent/",
        dataType: "xml",
        success: function(data) {
            var $i = 0;
            $(data).find('item').each(function() {
                $i++;
                var item = $(this);
                var date = $(item).find('pubDate').text();
                var title = $(item).find('title').text();
                var pubDate = $(item).find('pubDate').text();
                var description = $(item).find('description').text();
              
                var html = generateHtml(title,date,pubDate,description,"xml");
              $("#xml-feeds").append(html);
            });
            $("#result").hide();
        }
    }); 

function generateHtml(title,date,pubDate,description,type){
    var list_elem = $("<li class='list-group-item'></li>").html('<p class="modalTrigger"><span class="fa fa-comment fa-fw"></span> '  + 
            title + '<span class="pull-right text-muted small"><em>'+pubDate +'</em> </span></p> ');
    list_elem.attr("id", "list_elem");
    console.log(type);
    list_elem.append(
        $("<div class='modal fade' id='myModal' role='dialog'>").append(
            $("<div class='modal-dialog'>").append(
                $("<div class='modal-content'>").append("<div class='modal-header'><button type='button'" +
                   " class='close' data-dismiss='modal'>&times;</button></div>").append(type === "xml" ?
                        $("<div class='modal-body'>").html(description) : $("<div class='modal-body'>").text(description)
                ).append("<div class='modal-footer'><button type='button' class='btn btn-default'" +
                        "data-dismiss='modal'>Close</button></div>")
            )       
        )
    );
    return list_elem
}



