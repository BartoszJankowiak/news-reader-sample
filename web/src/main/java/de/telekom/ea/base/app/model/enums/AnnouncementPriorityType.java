package de.telekom.ea.base.app.model.enums;

/**
 * Announcement type enumeration.
 *
 * @author Razvan_Diaconu
 */
public enum AnnouncementPriorityType {

    /**
     * Announcement priority type Important.
     */
    IMPORTANT,

    /**
     * Announcement priority type Normal.
     */
    NORMAL,

    /**
     * Announcement priority type Low.
     */
    LOW;
}
