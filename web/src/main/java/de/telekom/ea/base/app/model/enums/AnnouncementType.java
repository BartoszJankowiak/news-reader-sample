package de.telekom.ea.base.app.model.enums;

/**
 * Announcement type enumeration.
 * 
 * @author Razvan_Diaconu
 */
public enum AnnouncementType {

    /**
     * Announcement type EVENT.
     */
    EVENT,

    /**
     * Announcement type INFO .
     */
    INFO,

    /**
     * Announcement type SYSTEM_MAINTENANCE .
     */
    SYSTEM_MAINTENANCE;

}
