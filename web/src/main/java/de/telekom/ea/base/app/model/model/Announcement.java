package de.telekom.ea.base.app.model.model;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import de.telekom.ea.base.app.model.enums.AnnouncementPriorityType;
import de.telekom.ea.base.app.model.enums.AnnouncementType;

/**
 * Announcement model.
 * 
 * @author Razvan_Diaconu
 *
 */
@XmlRootElement(name="Announcement")
@XmlAccessorType(XmlAccessType.FIELD)
public class Announcement implements Serializable{

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 4972014405280069254L;

    @XmlElement
    private AnnouncementType type;

    @XmlElement
    private String title;

    @XmlElement
    private String description;

    @XmlElement
    private AnnouncementPriorityType priority;

    @XmlElement
    private Date startDate;

    @XmlElement
    private Date expireDate;
    @XmlElement
    private boolean active;
    @XmlElement
    private String applicationId;

    public AnnouncementType getType() {
        return type;
    }

    public void setType(AnnouncementType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the announcement.
     *
     * @param title the new title of the announcement
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets the description of the announcement.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the description of the announcement.
     *
     * @param description the new description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets the priority type of the announcements.
     *
     * @return the priority
     */
    public AnnouncementPriorityType getPriority() {
        return priority;
    }

    /**
     * Sets the priority type of the announcement.
     *
     * @param priority the new priority type of the announcements
     */
    public void setPriority(AnnouncementPriorityType priority) {
        this.priority = priority;
    }

    /**
     * Gets the start date of the announcement.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets the start date of the announcement.
     *
     * @param startDate the new start date
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the expiration date.
     *
     * @return the expire date
     */
    public Date getExpireDate() {
        return expireDate;
    }

    /**
     * Sets the expiration date.
     *
     * @param expireDate the new expire date
     */
    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    /**
     * Gets the announcement activation status.
     *
     * @return true, if is active
     */
    public boolean isActive() {
        return active;
    }

    /**
     * Sets the announcement activation status.
     *
     * @param active the new active
     */
    public void setActive(boolean active) {
        this.active = active;
    }

    /**
     * Gets the application id.
     *
     * @return the application id
     */
    public String getApplicationId() {
        return applicationId;
    }

    /**
     * Sets the application id.
     *
     * @param applicationId the new application id
     */
    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

}
