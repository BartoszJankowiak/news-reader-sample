/*
 * JBoss, Home of Professional Open Source
 * Copyright 2015, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.jboss.as.quickstarts.kitchensink_ear.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.plexus.util.IOUtil;
import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;
import org.jboss.resteasy.plugins.providers.atom.Content;
import org.jboss.resteasy.plugins.providers.atom.Entry;
import org.jboss.resteasy.plugins.providers.atom.Feed;
import org.jboss.resteasy.plugins.providers.atom.Link;
import org.jboss.resteasy.plugins.providers.atom.Person;

import de.telekom.ea.base.app.model.enums.AnnouncementType;
import de.telekom.ea.base.app.model.model.Announcement;

/**
 * JAX-RS Example
 * <p/>
 * This class produces a RESTful service to read/write the contents of the members table.
 */
@Path("/news")
@RequestScoped
public class NewsExposerRESTService {

    @GET
    @Path("/getAnnouncement")
    @Produces("application/atom+xml")
    @Wrapped
    public Feed getAnnouncement() throws URISyntaxException {
        Feed feed = new Feed();
        feed.getEntries().addAll(createEntries());
        return feed;

    }

    private List<Entry> createEntries() {
        List<Entry> entries = new ArrayList<>();
        Announcement announcement = new Announcement();
        announcement.setActive(true);
        announcement.setDescription("decription feed 1");
        announcement.setStartDate(new Date());
        announcement.setTitle("Item titile 1");
        announcement.setType(AnnouncementType.INFO);
        entries.add(createEntryFromAnncouement(announcement));

        announcement = new Announcement();
        announcement.setActive(false);
        announcement.setDescription("decription feed 2");
        announcement.setStartDate(new Date());
        announcement.setTitle("Item title 2");
        announcement.setType(AnnouncementType.INFO);
        entries.add(createEntryFromAnncouement(announcement));

        return entries;
    }

    private Entry createEntryFromAnncouement(Announcement announcement) {
        Entry entry = new Entry();
        entry.setTitle(announcement.getTitle());
        entry.setPublished(announcement.getStartDate());
        Content content = new Content();
//        content.setText("test");
        content.setJAXBObject(announcement);
        content.setType(MediaType.APPLICATION_ATOM_XML_TYPE);
        entry.setContent(content);

        return entry;
    }

    @GET
    @Path("/getFeed")
    @Produces("application/atom+xml")
    @Wrapped
    public Feed getFeed() throws URISyntaxException {

        Feed feed = new Feed();
        feed.setId(new URI("http://example.com/42"));
        feed.setTitle("My Feed");
        feed.setUpdated(new Date());
        Link link = new Link();
        link.setHref(new URI("http://localhost"));
        link.setRel("edit");
        feed.getLinks().add(link);
        feed.getAuthors().add(new Person("Bill Burke"));
        Entry entry = new Entry();
        entry.setTitle("ass");
        Content content = new Content();
        content.setType(MediaType.TEXT_HTML_TYPE);
        content.setText("Nothing much");
        entry.setContent(content);
        feed.getEntries().addAll(getEntries());
        return feed;
    }
    
   

    public List<Entry> getEntries() {
        List<Entry> entries = new ArrayList<>();
        Entry entry = new Entry();
        entry.setTitle("Hello World");
        Content content = new Content();
        content.setType(MediaType.TEXT_HTML_TYPE);
        content.setText("Nothing much");
        entry.setContent(content);
        entries.add(entry);

        entry = new Entry();
        entry.setTitle("Thx for message");
        content = new Content();
        content.setType(MediaType.TEXT_HTML_TYPE);
        content.setText("But still sth");
        entry.setContent(content);
        entries.add(entry);

        entry = new Entry();
        entry.setTitle("Which is displayed");
        content = new Content();
        content.setType(MediaType.TEXT_HTML_TYPE);
        content.setText(">new not interesting msg");
        entry.setContent(content);
        entries.add(entry);
        return entries;

    }

    @XmlRootElement(name = "List")
    @XmlAccessorType(XmlAccessType.PROPERTY)
    public static class JaxbList<T> {
        @XmlElement(name = "Item")
        protected List<T> list;

        public JaxbList() {
        }

        public JaxbList(List<T> list) {
            this.list = list;
        }

        @XmlElement(name = "Item")
        public List<T> getList() {
            return list;
        }
    }

    @GET
    @Path("/export")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getFile() {

        try {
            byte[] fileContent = IOUtil.toByteArray(getClass().getClassLoader().getResource("RSSFeedXML.xml").openStream());
            ResponseBuilder response = Response.ok((Object) fileContent);
            response.header("Content-Disposition", "attachment; filename=newfile.xls");
            return response.build();

        } catch (IOException e) {
            ResponseBuilder response = Response.status(400);
            return response.build();
        }

    }

    @GET
    @Path("/exportContent")
    @Produces("application/atom+xml")
    public String getFileContent() {
        try {
            byte[] fileContent = IOUtil.toByteArray(getClass().getClassLoader().getResource("RSSFeedXML.xml").openStream());
            return new String(fileContent);

        } catch (IOException e) {
            return null;
        }

    }

}
