package org.jboss.as.quickstarts.kitchensink_ear.beans;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.component.html.HtmlOutputText;

@ManagedBean(name = "mBean")
@SessionScoped
public class NewsExposerBean implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7049130963668760045L;
    private String selectedFeed;
    private Object feed;

    public String getSelectedFeed() {
        return selectedFeed;
    }

    public Object getFeed() {
        return feed;
    }

    public void setFeed(Object feed) {
//        FeedReader f = (FeedReader) feed;
//        f.getVar();
        
        this.feed = feed;
    }

    public void setSelectedFeed(String selectedFeed) {
        this.selectedFeed = selectedFeed;
    }

    public void doSth(String event) {
        HtmlOutputText f= (HtmlOutputText) feed;
        f.getValue();
        System.out.println("sadas");
    }

}
